from rdflib import Graph, Literal, RDF, URIRef
from rdflib.namespace import SDO, DCTERMS

graph = Graph()

justin_welby = URIRef("http://example.org/Justin_Welby")

graph.add((justin_welby, RDF.type, SDO.Person))
graph.add((justin_welby, SDO.name, Literal("Justin Welby", lang="en")))

reimagining_britain = URIRef("http://example.org/Reimagining_Britain")

graph.add((reimagining_britain, RDF.type, SDO.Book))
graph.add((reimagining_britain, DCTERMS.title, Literal("Reimagining Britain", lang="en")))
graph.add((reimagining_britain, DCTERMS.creator, justin_welby))
graph.add((reimagining_britain, SDO.author, justin_welby))

# for s, p, o in graph:
#    print(s, p, o)

def main():
    print("Select subject to view:")
    subjects = list(enumerate(graph.subjects(None, None, unique=True), start=1))
    for id, subject in subjects:
        print(str(id) + ".", subject)
    chosen_id = int(input("> "))
    for id, subject in subjects:
        if chosen_id == id:
            for s, p, o in graph:
                if s == subject:
                    print(p, "->", o)

main()

# graph.bind("sdo", SDO)
# graph.bind("dcterms", DCTERMS)
# print(graph.serialize(format="n3"))